# Beykent üniversitesi Gömülü Sistem Tasarımı Dersi

Bu kod deposu gömülü sistem tasarımı ara sınavına karşın cevaplar içermektedir.

aşağıda yer alan sorular için Fritzing Diyagramını çizin ve  Arduino kodunu yazın.

### **Soru 1**
- Breadboard'a bir ultrasonik mesafe sensörü (HC-SR04) ve bir LCD (16 * 2) bağlayın. NewPing, LiquidCristal ve BarGraph kütüphanelerini kullanarak durum ekranlarını veya sensör okumalarını LCD ekrana göstermek için bir proje geliştirin. (Tüm kütüphaneler kullanılmalıdır)

### **Soru 2**
- Breadboard'unuza bir düğme(push button) ve 7 ‘li gösterge bağlayın. Düğmeye basıldığında 7’li göstergenin 9'dan 0'a kadar saymaya başladığı bir proje geliştirin. Düğmeye basılmadığında sayaç kapalı durumda olmalıdır.