
#define HC_TRIG 12
#define HC_ECHO 13
#define MAX_DISTANCE 200

#include <NewPing.h>
#include <LiquidCrystal.h>

NewPing sonar(HC_TRIG, HC_ECHO, MAX_DISTANCE);
LiquidCrystal lcd(7,6,5,4,3,2,1,0);
 
void setup(){
  pinMode(HC_TRIG, OUTPUT);  
  pinMode(HC_ECHO, INPUT);
  digitalWrite(HC_TRIG, LOW);
  
  lcd.begin(16,2);
  lcd.setCursor(0,0);

}

void loop(){
  delay(50);

  lcd.setCursor(0,1);
  lcd.print("Distance: ");
  lcd.setCursor(7,1);
  lcd.print(sonar.ping_cm());
  lcd.print(" cm");

  delay(100);
}